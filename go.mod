module gitee.com/LCTDDL/logrotator

go 1.18

require (
	github.com/lestrrat-go/strftime v1.0.6
	github.com/pkg/errors v0.9.1
)

require (
	github.com/stretchr/testify v1.7.1 // indirect
)
